-- ritorna il numero di righe di ing_gpsdev

SELECT COUNT(*) FROM ing_gpsdev;

-- ottenere USR da id dispositivo

SELECT U.login
FROM ing_vehicle V, ing_usr U
WHERE V.device_id=? AND U.actual_vehicle=V.plate;

-- ottenere RPA da USER

SELECT C.login
FROM ing_cfm C, ing_usr U
WHERE U.cfm=C.login AND U.login=?;

-- ottenere EXECUTIVE da RPA

SELECT E.login
FROM ing_executive E, ing_cfm C
WHERE C.login=? AND C.executive=E.login;

-- ottenere RPA da dispositivo

SELECT C.login
FROM ing_cfm C, ing_vehicle V, ing_usr U
WHERE V.device_id=? AND U.actual_vehicle=V.plate AND U.cfm=C.login;

-- ottenere EXECUTIVE da dispositivo

SELECT E.login
FROM ing_cfm C, ing_vehicle V, ing_usr U, Executive E
WHERE V.device_id=? AND U.actual_vehicle=V.plate AND U.cfm=C.login AND C.executive=E.login;

-- Salvare una posizione sulla base di dati
-- parametri in ordine: latitudine, longitudine, time, id dispositivo

INSERT INTO ing_gpsdev VALUES(?,?,?,?);

-- Salvare video sulla base di dati
-- parametri in ordine: login executive (usare query sopra), e url video
-- !!! salvarsi prima l'executive in una variabile!

INSERT INTO ing_video VALUES(?,?);

-- Ritornare tutte le posizioni di un veicolo ordinandole per data
-- crescente.
-- parametri in ordine:  id device, numero righe desiderate

SELECT G.latitude, G.longitude, G.id
FROM ing_gpsdev G
WHERE G.id=?
ORDER BY G.time DEC
LIMIT ?;

-- Ritornare tutti gli id dei device nella base di dati

SELECT id
FROM ing_device;

--ritornare tutti gli allarmi di un device dato il suo id

SELECT speed
FROM ing_alarms
WHERE device=?
ORDER BY speed DESC;

--ritornare il numero di telefono del cfm dal id del veicolo

SELECT C.tel_number
FROM ing_cfm C, ing_vehicle V, ing_usr U
WHERE V.device_id=? AND U.actual_vehicle=V.plate AND U.cfm=C.login;

--ritornare la email del cfm dal id del video

SELECT C.email
FROM ing_cfm C, ing_vehicle V, ing_usr U
WHERE V.device_id=? AND U.actual_vehicle=V.plate AND U.cfm=C.login;

--ritornare il nome del veicolo dal id del dispositivo

SELECT name
FROM ing_vehicle
WHERE device_id=?;

--ritorna il numero di telefono di uno user
--dato il suo login

SELECT tel_number
FROM ing_usr
WHERE login=?;

--ritorna l'email di uno user dato il suo login

SELECT email
FROM ing_usr
WHERE login=?;

--ritorna il login di un cfm di uno user dato il login dello user

SELECT cfm
FROM ing_usr
WHERE login=?;

--ritorna la targa del veicolo attuale di uno user dato il suo login

SELECT actual_vehicle
FROM ing_usr
WHERE login=?;

--ritorna tutte le posizioni di un veicolo tra due timestamp
--parametri in ordine sono il timestamp di inizio, quello di fine della ricerca e il login del guidatore attuale

SELECT G.latitude, G.longitude
FROM ing_gpsdev G, ing_usr U, ing_vehicle V
WHERE G.time > ? AND G.time < ? AND U.login=? AND U.actual_vehicle=V.plate AND V.device_id=G.id
ORDER BY time;

--ritorna l'ultima posizione di un veicolo dato lo user

SELECT G.latitude, G.longitude
FROM ing_gpsdev G, ing_usr U, ing_vehicle V
WHERE U.login=? AND U.actual_vehicle=V.plate AND V.device_id=G.id
ORDER BY G.time DESC
LIMIT 1;	

--modifica il numero di telefono di uno user
--i paramentri in ordine: il nuovo numero di telefono, il login dello user

UPDATE ing_usr
SET tel_number=?
WHERE login=?;

--modifica la email di uno user
--i paramentri in ordine: la nuova email, il login dello user

UPDATE ing_usr
SET email=?
WHERE login=?;

--ritorna il nome del veicolo attuale dello user

SELECT M.name
FROM ing_vehicle M, ing_usr U 
WHERE U.login=?
AND U.actual_vehicle=M.plate;






