-- CFM = CAR FLEET MANAGER = RPA
-- PLATE = TARGA
-- EXECUTIVE = DIRIGENTE

CREATE TABLE ing_device ( id CHAR(20) PRIMARY KEY);

CREATE TABLE ing_gpsdev (  latitude FLOAT, longitude FLOAT, time TIMESTAMP, id CHAR (20) REFERENCES ing_device(id) ON UPDATE CASCADE ON DELETE CASCADE, PRIMARY KEY ( latitude,longitude,time,id));

CREATE TABLE ing_vehicle ( plate CHAR(10) PRIMARY KEY,
		       device_id CHAR(20) REFERENCES ing_device(id) ON UPDATE CASCADE ON DELETE CASCADE,
		       name CHAR(20),
                       cfm CHAR(20) REFERENCES ing_cfm(login) ON UPDATE CASCADE ON DELETE CASCADE);	       

CREATE TABLE ing_usr ( login CHAR(20) PRIMARY KEY,
		   passwd CHAR(20) NOT NULL,
		   tel_number CHAR(50) NOT NULL,
		   email CHAR(50) NOT NULL,
		   cfm CHAR(20) REFERENCES ing_cfm(login) ON UPDATE CASCADE ON DELETE CASCADE,
                   actual_vehicle CHAR(10) REFERENCES ing_vehicle(plate) ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE ing_cfm ( login CHAR(20) PRIMARY KEY,
		   passwd CHAR(20) NOT NULL,
		   tel_number CHAR(50) NOT NULL,
		   email CHAR(50) NOT NULL,
		   executive CHAR(20) REFERENCES ing_executive(login) ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE ing_executive ( login CHAR(20) PRIMARY KEY,
			 passwd CHAR(20) NOT NULL);

CREATE TABLE ing_video ( executive CHAR(20) REFERENCES ing_executive(login)  ON UPDATE CASCADE ON DELETE CASCADE,
			 url VARCHAR(300) PRIMARY KEY
			 car_plate CHAR(10) REFERENCES ing_vehicle(plate) ON UPDATE CASCADE ON DELETE CASCADE );

CREATE TABLE ing_alarms ( speed INTEGER,
			  device CHAR(20) REFERENCES ing_device(id) ON UPDATE CASCADE ON DELETE CASCADE,
			  PRIMARY KEY(speed,device),
			  CHECK (speed>0));
		   

		      	
			



