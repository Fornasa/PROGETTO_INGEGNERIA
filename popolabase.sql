INSERT INTO ing_device VALUES ('DISP-000');
INSERT INTO ing_device VALUES ('DISP-001');
INSERT INTO ing_device VALUES ('DISP-002');
INSERT INTO ing_device VALUES ('DISP-003');

INSERT INTO ing_vehicle VALUES ('VR393FF','DISP-000','Maserati2014','cfm1login');
INSERT INTO ing_vehicle VALUES ('VR392FDM','DISP-001','AstonMartin2013','cfm1login');
INSERT INTO ing_vehicle VALUES ('VR293AAL','DISP-002','Lamborghini2015','cfm2login');
INSERT INTO ing_vehicle VALUES ('VR234AS','DISP-003','McLaren2012','cfm2login');

INSERT INTO ing_executive VALUES ('bosslogin','bosspassword');

INSERT INTO ing_cfm VALUES ('cfm1login','cfm1password','3491234567','cfm1@email.com','bosslogin');
INSERT INTO ing_cfm VALUES ('cfm2login','cfm2password','3339976583','cfm2@email.com','bosslogin');

INSERT INTO ing_usr VALUES ('francesco','fornasa','3491233456','francesco@mail.com','cfm1login','VR393FF');
INSERT INTO ing_usr VALUES ('federico','di_marco','3332957399','federico@mail.com','cfm1login','VR392FDM');
INSERT INTO ing_usr VALUES ('amin','ait_lamqadem','3569985432','amin@mail.com','cfm2login','VR293AAL');
INSERT INTO ing_usr VALUES ('andrea','saviozzi','0874433234','andrea@mai.com','cfm2login','VR234AS');

INSERT INTO ing_alarms VALUES ('50','DISP-000');
INSERT INTO ing_alarms VALUES ('100','DISP-000');
INSERT INTO ing_alarms VALUES ('140','DISP-000');
INSERT INTO ing_alarms VALUES ('80','DISP-001');
INSERT INTO ing_alarms VALUES ('30','DISP-002');
INSERT INTO ing_alarms VALUES ('100','DISP-002');
