package webserver.model;

import device.coordinates.Coordinates;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class WebDataSource {
	//per implementare il singleton
	private static WebDataSource instance = null;

	private final String user = "utentebasi";
	private final String passwd = "aminsavio";

	// URL per la connessione alla base di dati e' formato dai seguenti
	// componenti: <protocollo>://<host del server>/<nome base di dati>.
	private final String url = "jdbc:postgresql://81.4.104.73:5432/basidb";

	// Driver da utilizzare per la connessione e l'esecuzione delle query.
	private final String driver = "org.postgresql.Driver";
	
	//Query per ottenere il numero di telefono di uno user dato il suo login
	private final String queryTelNumber="SELECT tel_number "+
			"FROM ing_usr "+
			"WHERE login=?";
	
	//Query per ottenere l'email di uno user dato il suo login
	private final String queryEmail="SELECT email "+
				"FROM ing_usr "+
				"WHERE login=?";
	
	//Query per ottenere il login del cfm di uno user dato il login dello user
	private final String queryCfm="SELECT cfm "+
				"FROM ing_usr "+
				"WHERE login=?";
	
	//Query per ottenere la targa del veicolo attuale di uno user dato il login dello user
	private final String queryVehicle="SELECT actual_vehicle "+
				"FROM ing_usr "+
				"WHERE login=?";
	
	//Query per trovare tutte le posizioni tra due timestamp di un veicolo
	private final String queryCoordinatesBetween="SELECT G.latitude, G.longitude "+
			"FROM ing_gpsdev G, ing_usr U, ing_vehicle V "+
			"WHERE G.time > ? AND G.time < ? AND U.login=? AND U.actual_vehicle=V.plate AND V.device_id=G.id "+
			"ORDER BY time";
		
	 //Query per ottenere l'ultima posizione di un veicolo dato lo user
    private final String queryLastPos = "SELECT G.latitude, G.longitude "+
    		"FROM ing_gpsdev G, ing_usr U, ing_vehicle V "+
    		"WHERE U.login=? AND U.actual_vehicle=V.plate AND V.device_id=G.id  "+
    		"ORDER BY G.time DESC "+
    		"LIMIT 1";
    
    //Query per modificare il numero di telefono di uno user
    private final String queryChangeTelNumber = "UPDATE ing_usr "+
    		"SET tel_number=? "+
    		"WHERE login=?";
    
    //Query per modificare la email di uno user
    private final String queryChangeEmail = "UPDATE ing_usr "+
    		"SET email=? "+
    		"WHERE login=?";
	
    //Query per ottenere il nome del veicolo attuale di uno user
    private final String queryVehicleName = "SELECT M.name "+
    		"FROM ing_vehicle M, ing_usr U "+
    		"WHERE U.login=? "+
    		"AND U.actual_vehicle=M.plate ";

	//Query per ottenere i login dei guidatori dato il CPM
	private final String queryCpmtoLoginGuidatori = "SELECT login \n" +
			"from ing_usr \n" +
			"where cfm = ? ";

	private final String queryLogin = "SELECT login,passwd,ruolo \n" +
			"FROM (SELECT login, passwd, 1 as ruolo\n" +
			"      from ing_cfm\n" +
			"      UNION\n" +
			"      SELECT login, passwd, 0 as ruolo\n" +
			"      from ing_executive\n" +
			"      UNION\n" +
			"      SELECT login, passwd, 2 as ruolo\n" +
			"      from ing_usr) as lista_utenti\n" +
			"WHERE login = ? and passwd = ? ";

	private final String queryInsertGuidatore = "INSERT INTO ing_usr\n" +
			"VALUES (?,?,?,?,?,?);";

	private final String queryLimiti = "SELECT speed, device, plate, name, cfm\n" +
			"from ing_alarms INNER JOIN ing_vehicle on device_id=device\n" +
			"where plate = ? ";

	private final String queryCancellaLimite = "DELETE\n" +
			"from ing_alarms a\n" +
			"USING ing_vehicle v\n" +
			"where device_id=device\n" +
			"      AND a.speed = ? AND v.plate= ? ";

	private final String queryDeviceFromPlate = "SELECT device_id,plate\n" +
			"FROM ing_vehicle\n" +
			"where plate = ? ";

	private final String queryAggiungiLimite = "INSERT INTO ing_alarms\n" +
			"VALUES (?,?)";

	private final String queryLoginFromPlate = "SELECT *\n" +
			"FROM ing_vehicle INNER JOIN ing_usr\n" +
			"ON ing_vehicle.plate = ing_usr.actual_vehicle\n" +
			"WHERE plate =  ? ";

	//Query per ottenere l'executive dall'id del dispositivo
	private final String queryGetExecutiveFromId = "SELECT E.login " +
			"FROM ing_cfm C, ing_vehicle V, ing_usr U, ing_executive E " +
			"WHERE V.device_id=? AND U.actual_vehicle=V.plate AND U.cfm=C.login AND C.executive=E.login ";

	//Query per salvare il video sulla base di dati
	private final String queryStoreVideo = "INSERT INTO ing_video VALUES (?,?,?)";

	//Query per ottenere i CPM dato un executive
	private final String queryGetCpmFromExecutive = "SELECT c.*\n" +
			"FROM ing_executive e INNER JOIN ing_cfm as c\n" +
			"ON e.login = c.executive" +
			" WHERE c.executive = ? ";
	
	//Query per ottenere tutti i video di un executive
	private final String queryVideosFromExecutive = "SELECT VID.url, VE.name " +
			"FROM ing_video VID, ing_vehicle VE " +
			"WHERE VID.executive=? AND VID.car_plate=VE.plate ";
	
	//Query per ottenere la tagra dall' id di un dispositivo 
	private final String queryGetPlateFromId = "SELECT plate FROM ing_vehicle WHERE device_id=? ";


	//==================================METDOI===================================================
	   
    private WebDataSource() throws ClassNotFoundException {
        try{
        	Class.forName( driver );
        }catch(ClassNotFoundException e){
        	e.printStackTrace();
        }
      }
    
    public static WebDataSource getWebDataSource() throws ClassNotFoundException {
        if (instance == null) {
            instance = new WebDataSource();
        }
        return instance;
    }
	//============================================================================================

	public static void main(String[] args) throws ClassNotFoundException {
		WebDataSource ds = WebDataSource.getWebDataSource();
		System.out.print("IL NUMERO DI TELEFONO ATTUALE DI FRANCESCO: ");
		String actual_number = ds.getTelNumberFromId("francesco");
		System.out.println(actual_number);
		System.out.println("IL NUMERO DI TELEFONO VIENE CAMBIATO CON 123456");
		ds.changeTelNumberFromId("123456", "francesco");
		System.out.println("ORA IL NUMERO DI FRANCESCO E': " + ds.getTelNumberFromId("francesco"));
		System.out.println("IL NUMERO È STATO SETTATO CORRETTAMENTE?" + "123456".equals(ds.getTelNumberFromId("francesco")));
		System.out.println("RESETTO IL NUMERO CORRETTO");
		ds.changeTelNumberFromId(actual_number, "francesco");
		System.out.println("=======================================================");
		System.out.print("EMAIL ATTUALE DI FRANCESCO: ");
		String actual_email = ds.getEmailFromId("francesco");
		System.out.println(actual_email);
		System.out.println("L'EMAIL VIENE CAMBIATO CON nuovo@mail.com");
		ds.changeEmailFromId("nuovo@mail.com", "francesco");
		System.out.println("ORA L'EMAIL DI FRANCESCO E': " + ds.getEmailFromId("francesco"));
		System.out.println("L'EMAIL È STATA SETTATA CORRETTAMENTE?" + "nuovo@mail.com".equals(ds.getEmailFromId("francesco")));
		System.out.println("RESETTO L'EMAIL CORRETTA");
		ds.changeEmailFromId(actual_email, "francesco");
		System.out.println("=======================================================");
		System.out.println("IL LOGIN DEL CFM DI FRANCESCO: " + ds.getCfmFromId("francesco"));
		System.out.println("=======================================================");
		System.out.println("LA TARGA DEL VEICOLO ATTUALE DI FRANCESCO: " + ds.getVehicleFromId("francesco"));
		System.out.println("=======================================================");
		System.out.println("L'ULTIMA POSIZIONE DI FRANCESCO: " + ds.getLastPosition("francesco"));
		System.out.println("=======================================================");
		System.out.println("IL NOME DEL VEICOLO DI FRANCESCO: " + ds.getVehicleNameFromId("francesco"));
		System.out.println("=======================================================");
		System.out.print("TUTTE LE POSIZIONI DI FRANCESCO TRA ");
		Timestamp now = new Timestamp(System.currentTimeMillis());
		String today = now.toString().substring(0, 10);
		Timestamp begin = Timestamp.valueOf(today + " 09:00:00");
		Timestamp end = Timestamp.valueOf(today + " 12:00:00");
		System.out.print(begin + " E " + end + " :\n");
		for (Coordinates c : ds.getCoordinatesBetween(begin, end, "francesco"))
			System.out.println("\t-" + c);
		System.out.println("=======================================================");

	}


	public List<Cpm> getCpmFromExecutive(String executive) {
		List<Object> args = new ArrayList<Object>();
		args.add(executive);

		List<Cpm> result = new ArrayList<Cpm>();
		ResultSet rs = executeQuery(queryGetCpmFromExecutive, args, false);
		try {
			while (rs.next()) {
				Cpm c = new Cpm();

				c.setLogin(rs.getString("login").trim());
				c.setTel_number(rs.getString("tel_number").trim());
				c.setEmail(rs.getString("email").trim());
				c.setExecutive(rs.getString("executive").trim());

				result.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}




	public void storeVideo(String videoPath, String deviceId) {
		List<Object> args = new ArrayList<Object>();
		String executive = "";
		args.add(deviceId);

		//PER PRIMA COSA RECUPER L'EXECUTIVE DALL' ID
		ResultSet rs1 = executeQuery(queryGetExecutiveFromId, args, false);
		try {
			if (rs1.next())
				executive = rs1.getString("login");
		} catch (SQLException sqle) { // Catturo le eventuali eccezioni
			sqle.printStackTrace();
		}
		
		//PER PRIMA COSA RECUPERA LA TARGA DALL' ID
		List<Object> args2 = new ArrayList<Object>();
		String plate ="";
		args2.add(deviceId);
		ResultSet rs2 = executeQuery(queryGetPlateFromId, args2, false);
		try {
			if (rs2.next())
				plate = rs2.getString("plate");
			} catch (SQLException sqle) { // Catturo le eventuali eccezioni
				sqle.printStackTrace();
		}

		//CONTROLLO DI ESSERE RIUSCITO A TROVARE L'EXECUTIVE
		if (executive == null)
			return;
		//USO L'EXECUTIVE PER POPOLARE LA TABELLA

		args = new ArrayList<Object>();
		args.add(executive);
		args.add(videoPath);
		args.add(plate);
		executeQuery(queryStoreVideo, args, true);
	}

	public String getLoginFromPlate(String plate) {
		List<Object> args = new ArrayList<Object>();
		args.add(plate);

		String result = null;
		ResultSet rs = executeQuery(queryLoginFromPlate, args, false);
		try {
			if (rs.next()) {
				result = rs.getString("login").trim();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public void aggiungiLimite(String limite, String plate) throws ClassNotFoundException {
		List<Object> args = new ArrayList<Object>();

		Integer i = Integer.valueOf(limite);
		args.add(i);
		args.add(WebDataSource.getWebDataSource().getDeviceFromPlate(plate));

		executeQuery(queryAggiungiLimite, args, true);
	}

	public String getDeviceFromPlate(String plate) {
		List<Object> args = new ArrayList<Object>();
		args.add(plate);

		String result = null;
		ResultSet rs = executeQuery(queryDeviceFromPlate, args, false);
		try {
			if (rs.next()) {
				result = rs.getString("device_id").trim();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public void cancellaLimite(String limite, String plate) {
		List<Object> args = new ArrayList<Object>();

		Integer i = Integer.valueOf(limite);
		args.add(i);
		args.add(plate);

		executeQuery(queryCancellaLimite, args, true);
	}

	public List<String> getLimiti(String plate) {
		List<Object> args = new ArrayList<Object>();
		args.add(plate);

		List<String> result = new ArrayList<String>();
		ResultSet rs = executeQuery(queryLimiti, args, false);
		try {
			while (rs.next()) {
				result.add(rs.getString("speed").trim());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<String> getGuidatoreLoginFromCpm(String login) {
		List<Object> args = new ArrayList<Object>();
		args.add(login);

		List<String> result = new ArrayList<String>();
		ResultSet rs = executeQuery(queryCpmtoLoginGuidatori, args, false);
		try {
			while (rs.next()) {
				result.add(rs.getString("login").trim());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public String getLogin(String login, String passwd) {
		List<Object> args = new ArrayList<Object>();
		args.add(login);
		args.add(passwd);
		ResultSet rs = executeQuery(queryLogin, args, false);
		String result = null;
		try {
			if (rs.next()) {
				result = rs.getString("ruolo");
				System.out.println("Utente trovato: " + result);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public String getTelNumberFromId(String id){
		List<Object> args = new ArrayList<Object>();
		args.add(id);
		String result=null;
		ResultSet rs = executeQuery(queryTelNumber,args,false);
		try{
			if(rs.next()){
				result=rs.getString("tel_number");
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return result.trim();
	}
	
	public String getEmailFromId(String id){
		List<Object> args = new ArrayList<Object>();
		args.add(id);
		String result=null;
		ResultSet rs = executeQuery(queryEmail,args,false);
		try{
			if(rs.next()){
				result=rs.getString("email");
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return result.trim();
	}

	public String getCfmFromId(String id){
		List<Object> args = new ArrayList<Object>();
		args.add(id);
		String result=null;
		ResultSet rs = executeQuery(queryCfm,args,false);
		try{
			if(rs.next()){
				result=rs.getString("cfm");
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return result.trim();
	}
	
	public String getVehicleFromId(String id){
		List<Object> args = new ArrayList<Object>();
		args.add(id);
		String result=null;
		ResultSet rs = executeQuery(queryVehicle,args,false);
		try{
			if(rs.next()){
				result=rs.getString("actual_vehicle");
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return result.trim();
	}
	
	public String getVehicleNameFromId(String id){
		List<Object> args = new ArrayList<Object>();
		args.add(id);
		String result=null;
		ResultSet rs = executeQuery(queryVehicleName,args,false);
		try{
			if(rs.next()){
				result=rs.getString("name");
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return result.trim();
	}
	
	public void changeTelNumberFromId(String newNumber,String id){
		List<Object> args = new ArrayList<Object>();
		args.add(newNumber);
		args.add(id);
		executeQuery(queryChangeTelNumber,args,true);
	}
	
	public void changeEmailFromId(String newEmail,String id){
		List<Object> args = new ArrayList<Object>();
		args.add(newEmail);
		args.add(id);
		executeQuery(queryChangeEmail, args, true);
	}
	
	/**
	 * Ritorna una lista contenete le coordinate di un veicolo tra due tempi
	 * @param begin il timestamp di inizio della ricerca
	 * @param end il timestamp di fine della ricerca
	 * @param id il login del guidatore
	 * @return la lista di coordinate
	 */
	public List<Coordinates> getCoordinatesBetween(Timestamp begin, Timestamp end, String id){
		List<Object> args = new ArrayList<Object>();
		args.add(begin);
		args.add(end);
		args.add(id);
		List<Coordinates> result=new ArrayList<Coordinates>();
		ResultSet rs = executeQuery(queryCoordinatesBetween,args,false);
		try{
			while(rs.next()){
				result.add(new Coordinates(rs.getFloat("latitude"),rs.getFloat("longitude")));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return result;
	}
	
	public Coordinates getLastPosition(String id){
		List<Object> args = new ArrayList<Object>();
		args.add(id);
		Coordinates result=null;
		ResultSet rs = executeQuery(queryLastPos,args,false);
		try{
			if(rs.next()){
				result= new Coordinates(rs.getFloat("latitude"),rs.getFloat("longitude"));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return result;
	}
	
	public List<Video> getVideosFromExecutive(String executive){
		List<Object> args = new ArrayList<Object>();
		args.add(executive);
		List<Video> result = new ArrayList<Video>();
		ResultSet rs = executeQuery(queryVideosFromExecutive,args,false);
		try{
			while (rs.next())
				result.add(new Video(rs.getString("url"),rs.getString("name")));
		}catch (SQLException e){
			e.printStackTrace();
		}
		
		return result;
		}
		
		

	public boolean insertGuidatore(String login, String passwd, String tel_number, String email, String cfm, String actual_vehicle) {
		List<Object> args = new ArrayList<Object>();
		args.add(login);
		args.add(passwd);
		args.add(tel_number);
		args.add(email);
		args.add(cfm);
		args.add(actual_vehicle);

		executeQuery(queryInsertGuidatore, args, true);

		return true;

	}
	
	//===============TEST MAIN=====================

	/**
	 * @param query    la query da eseguire
	 * @param args     la lista di parametri in caso usare lista vuota. NB castare a container
	 * @param isUpdate se la query è una update
	 * @return se la query è una update ritorna null
	 */
	private ResultSet executeQuery(String query, List<Object> args, boolean isUpdate) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int index = 1;

		try {
			// tentativo di connessione al database
			con = DriverManager.getConnection(url, user, passwd);
			// connessione riuscita, ottengo l'oggetto per l'esecuzione dell'interrogazione.
			pstmt = con.prepareStatement(query);
			pstmt.clearParameters();
			// imposto i parametri della query
			for (Object param : args) {
				if (param.getClass().getSimpleName().equals("Integer"))
					pstmt.setInt(index, (int) param);
				else if (param.getClass().getSimpleName().equals("Float"))
					pstmt.setFloat(index, (float) param);
				else if (param.getClass().getSimpleName().equals("Timestamp"))
					pstmt.setTimestamp(index, (Timestamp) param);
				else if (param.getClass().getSimpleName().equals("Double")) {
					Double d = (Double) param;
					pstmt.setFloat(index, d.floatValue());
				} else {
					pstmt.setString(index, (String) param);
				}
				index++;
			}
			if (isUpdate)
				pstmt.executeUpdate();
			else
				rs = pstmt.executeQuery();

		} catch (SQLException sqle) { // Catturo le eventuali eccezioni
			sqle.printStackTrace();

		} finally { // alla fine chiudo la connessione.
			try {
				con.close();
			} catch (SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return rs;

	}

}






















