package webserver.model;

import device.coordinates.Coordinates;
import webserver.controller.LoginHandle;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@ManagedBean
@SessionScoped
public class GuidatoreBean implements Serializable {
	//String associato al ruolo
	private final String ruolo = "2";

	private WebDataSource ds;
	private LoginHandle loginHandle;
	private String login;
	private String tel_number;
	private String email;
	private String cfm;
	private String vehiclePlate;
	private String vehicleName;


	public GuidatoreBean() throws ClassNotFoundException, IOException {

		//Ottengo il bean
		FacesContext context = FacesContext.getCurrentInstance();
		loginHandle = (LoginHandle) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userLogin");

		System.out.println(loginHandle);

		if (loginHandle != null)
			login = loginHandle.handshakeLogin(ruolo);

		//Se il bean non esiste oppure l'utente non � corretto/valido allora redirect
		if (login == null || loginHandle == null) {
			System.out.println("Errore Auth Guidatore:" + login + loginHandle);
			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
			FacesContext.getCurrentInstance().getExternalContext().redirect("index.jsf");
		}
		else {
			ds = WebDataSource.getWebDataSource();
			tel_number = null;
			email = null;
			cfm = null;
			vehiclePlate = null;
		}
	}


	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getTel_number() {
		if( tel_number == null)
			tel_number = ds.getTelNumberFromId(login);
		return tel_number;
	}

	public void setTel_number(String tel_number) {
		ds.changeTelNumberFromId(tel_number, login);
		this.tel_number = tel_number;
	}

	public String getEmail() {
		if(email == null)
			email = ds.getEmailFromId(login);
		return email;
	}

	public void setEmail(String email) {
		ds.changeEmailFromId(email, login);
		this.email = email;
		System.out.println("Email cambiata");
	}

	public String getCfm() {
		if (cfm == null)
			cfm = ds.getCfmFromId(login);
		return cfm;
	}

	public String getVehiclePlate() {
		if( vehiclePlate==null)
			vehiclePlate = ds.getVehicleFromId(login);
		return vehiclePlate;
	}
	
	public String getVehicleName(){
		if(vehicleName == null)
			vehicleName = ds.getVehicleNameFromId(login);
		return vehicleName;
	}
	
	public Coordinates getLastPosition(){
		return ds.getLastPosition(login);
	}
	
	public List<Coordinates> getPositionsBetween (Timestamp begin, Timestamp end){
		//System.out.println("Invocato getPositionBetween");
		return ds.getCoordinatesBetween(begin, end,login);
	}

}
