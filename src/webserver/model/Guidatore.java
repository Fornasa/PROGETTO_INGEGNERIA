package webserver.model;

import device.coordinates.Coordinates;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;


public class Guidatore implements Serializable {
    private WebDataSource ds;
    private String login;
    private String tel_number;
    private String email;
    private String cfm;
    private String vehiclePlate;
    private String vehicleName;
    private String device;

    public Guidatore() throws ClassNotFoundException, IOException {
        ds = WebDataSource.getWebDataSource();
        login = "null";
        tel_number = null;
        email = null;
        cfm = null;
        vehiclePlate = null;
        vehicleName = null;
    }


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getTel_number() {
        if (tel_number == null)
            tel_number = ds.getTelNumberFromId(login);
        return tel_number;
    }

    public void setTel_number(String tel_number) {
        ds.changeTelNumberFromId(tel_number, login);
        this.tel_number = tel_number;
    }

    public String getEmail() {
        if (email == null)
            email = ds.getEmailFromId(login);
        return email;
    }

    public void setEmail(String email) {
        ds.changeEmailFromId(email, login);
        this.email = email;
        System.out.println("Email cambiata");
    }

    public String getCfm() {
        if (cfm == null)
            cfm = ds.getCfmFromId(login);
        return cfm;
    }

    public String getVehiclePlate() {
        if (vehiclePlate == null)
            vehiclePlate = ds.getVehicleFromId(login);
        return vehiclePlate;
    }

    public void setVehiclePlate(String vehiclePlate) {
        this.vehiclePlate = vehiclePlate;
    }

    public String getVehicleName() {
        if (vehicleName == null)
            vehicleName = ds.getVehicleNameFromId(login);
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public Coordinates getLastPosition() {
        return ds.getLastPosition(login);
    }

    public List<Coordinates> getPositionsBetween(Timestamp begin, Timestamp end) {
        return ds.getCoordinatesBetween(begin, end, login);
    }


    public String getDevice() {
        if (device == null)
            device = ds.getDeviceFromPlate(getVehiclePlate());
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

}
