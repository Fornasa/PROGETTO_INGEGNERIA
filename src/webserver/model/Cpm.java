package webserver.model;

import java.io.IOException;
import java.io.Serializable;


public class Cpm implements Serializable {
    private WebDataSource ds;

    private String login;
    private String tel_number;
    private String email;
    private String executive;

    public Cpm() throws ClassNotFoundException, IOException {
        ds = WebDataSource.getWebDataSource();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getTel_number() {
        return tel_number;
    }

    public void setTel_number(String tel_number) {
        this.tel_number = tel_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExecutive() {
        return executive;
    }

    public void setExecutive(String executive) {
        this.executive = executive;
    }
}
