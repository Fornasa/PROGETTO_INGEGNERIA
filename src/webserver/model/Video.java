package webserver.model;

import java.io.Serializable;


public class Video implements Serializable {

    private String url;
    private String macchina;

    public Video() {
    }

    public Video(String url, String macchina) {
        this.url = url;
        this.macchina = macchina;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMacchina() {
        return macchina;
    }

    public void setMacchina(String macchina) {
        this.macchina = macchina;
    }
}
