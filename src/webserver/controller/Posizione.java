package webserver.controller;

import device.coordinates.Coordinates;
import org.primefaces.context.RequestContext;
import org.primefaces.model.map.MapModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;

@ManagedBean
@SessionScoped
public class Posizione implements Serializable {

    @ManagedProperty(value = "#{macchinaView}")
    MacchinaView macchinaView;

    private MapModel simpleModel;


    public void setMacchinaView(MacchinaView macchinaView) {
        this.macchinaView = macchinaView;
    }

    @PostConstruct
    public void init() {

    }

    public MapModel getSimpleModel() {
        return simpleModel;
    }

    public void save() {

        //System.out.println("Chiamato Save di Posizione.java");

        Coordinates lastPosition = macchinaView.getLastPosition();
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("longitudine", lastPosition.getLongitude());
        context.addCallbackParam("latitudine", lastPosition.getLatitude());
    }


}