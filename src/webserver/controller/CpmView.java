package webserver.controller;


import webserver.model.WebDataSource;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;

@ManagedBean
@SessionScoped
public class CpmView implements Serializable {
    //String associato al ruolo
    private final String ruolo = "1";

    private WebDataSource ds;
    private LoginHandle loginHandle;
    private String login;
    private String tel_number;
    private String email;
    private String executive;


    public CpmView() throws ClassNotFoundException, IOException {

        //Ottengo il bean
        FacesContext context = FacesContext.getCurrentInstance();
        loginHandle = (LoginHandle) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userLogin");

        System.out.println(loginHandle);

        if (loginHandle != null)
            login = loginHandle.handshakeLogin(ruolo);

        //Se il bean non esiste oppure l'utente non � corretto/valido allora redirect
        if (login == null || loginHandle == null) {
            System.out.println("Errore Auth Guidatore:" + login + loginHandle);
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.jsf");
        } else {
            ds = WebDataSource.getWebDataSource();
            tel_number = null;
            email = null;
            executive = null;
        }
    }


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getTel_number() {
        if (tel_number == null)
            tel_number = ds.getTelNumberFromId(login);
        return tel_number;
    }

    public void setTel_number(String tel_number) {
        ds.changeTelNumberFromId(tel_number, login);
        this.tel_number = tel_number;
    }

    public String getEmail() {
        if (email == null)
            email = ds.getEmailFromId(login);
        return email;
    }

    public void setEmail(String email) {
        ds.changeEmailFromId(email, login);
        this.email = email;
    }


}
