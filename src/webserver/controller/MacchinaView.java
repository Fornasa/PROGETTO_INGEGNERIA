package webserver.controller;

import device.coordinates.Coordinates;
import webserver.model.WebDataSource;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@ManagedBean
@SessionScoped
public class MacchinaView implements Serializable {

    private WebDataSource ds;
    private String login;
    private String tel_number;
    private String email;
    private String cfm;

    private String vehicleName;
    private String vehiclePlate;
    private String device;


    public MacchinaView() throws ClassNotFoundException, IOException {
        ds = WebDataSource.getWebDataSource();

        login = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userLogin");

    }

    public String outcome() throws ClassNotFoundException {

        login = null;
        ds = WebDataSource.getWebDataSource();
        tel_number = null;
        email = null;
        cfm = null;
        vehiclePlate = null;
        device = null;
        vehicleName = null;

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        vehiclePlate = params.get("vehiclePlate");
        login = WebDataSource.getWebDataSource().getLoginFromPlate(vehiclePlate);

        System.out.println(this.getClass().getName() + " Targa inject: " + vehiclePlate);

        return "macchina?faces-redirect=true";
    }

    public String getDevice() {
        if (device == null) {
            device = ds.getDeviceFromPlate(getVehiclePlate());
            System.out.println(this.getClass().getName() + " Device recuperato: " + device);
        }
        return device;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getTel_number() {
        if (tel_number == null)
            tel_number = ds.getTelNumberFromId(login);
        return tel_number;
    }

    public void setTel_number(String tel_number) {
        ds.changeTelNumberFromId(tel_number, login);
        this.tel_number = tel_number;
    }

    public String getEmail() {
        if (email == null)
            email = ds.getEmailFromId(login);
        return email;
    }

    public void setEmail(String email) {
        ds.changeEmailFromId(email, login);
        this.email = email;
        System.out.println("Email cambiata");
    }

    public String getCfm() {
        if (cfm == null) {
            try {
                cfm = ds.getCfmFromId(login);
            } catch (NullPointerException e) {
            }
        }
        return cfm;
    }

    public String getVehiclePlate() {

        return ds.getVehicleFromId(login);
    }

    public String getVehicleName() {
        if (vehicleName == null)
            vehicleName = ds.getVehicleNameFromId(login);
        return vehicleName;
    }

    public Coordinates getLastPosition() {
        return ds.getLastPosition(login);
    }

    public List<Coordinates> getPositionsBetween(Timestamp begin, Timestamp end) {
        //System.out.println("Invocato getPositionBetween");
        System.out.println(this.getClass().getName() + "login GPS: " + login);
        return ds.getCoordinatesBetween(begin, end, login);
    }

}
