package webserver.controller;

import org.primefaces.context.RequestContext;
import webserver.model.WebDataSource;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ManagedBean
@SessionScoped
public class ImpostaLimiti implements Serializable {

    String targa;
    WebDataSource ds;
    private List<String> limiti;
    private List<String> limitiSelezionati;
    private String nuovoLimite;

    public void guardaLimiti(String targa) {

        this.targa = targa;
        try {
            ds = WebDataSource.getWebDataSource();
            limiti = ds.getLimiti(targa);

            Map<String, Object> options = new HashMap<String, Object>();
            options.put("modal", true);
            options.put("dynamic", true);

            RequestContext.getCurrentInstance().openDialog("impostaLimiti", options, null);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    public List<String> getLimiti() {
        return limiti;
    }

    public List<String> getLimitiSelezionati() {
        return limitiSelezionati;
    }

    public void setLimitiSelezionati(List<String> limitiSelezionati) {
        this.limitiSelezionati = limitiSelezionati;
    }

    public void cancellaLimiti() {
        //Cancello i limiti
        for (String limite : limitiSelezionati) {
            ds.cancellaLimite(limite, targa);
            limiti.remove(limite);
        }
    }

    public void aggiungiLimite() {
        try {
            ds.aggiungiLimite(nuovoLimite, targa);
            limiti.add(nuovoLimite);
            nuovoLimite = null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getNuovoLimite() {
        return nuovoLimite;
    }

    public void setNuovoLimite(String nuovoLimite) {
        this.nuovoLimite = nuovoLimite;
    }
}