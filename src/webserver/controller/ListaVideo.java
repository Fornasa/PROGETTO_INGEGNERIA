package webserver.controller;

import webserver.model.Video;
import webserver.model.WebDataSource;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean
@ViewScoped

public class ListaVideo implements Serializable {

    WebDataSource ds;
    private List<Video> listaVideo;
    private Video videoSelezionato;

    @PostConstruct
    public void init() {

        try {
            ds = WebDataSource.getWebDataSource();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        listaVideo = ds.getVideosFromExecutive("bosslogin");
    }

    public List<Video> getListaVideo() {
        return listaVideo;
    }

    public Video getSelectedVideo() {
        return videoSelezionato;
    }

    public void setSelectedVideo(Video videoSelezionato) {
        this.videoSelezionato = videoSelezionato;
    }

    public void cancellaVideo(Video video) {
        listaVideo.remove(video);
    }
}