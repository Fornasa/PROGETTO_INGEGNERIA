package webserver.controller;

import org.primefaces.context.RequestContext;
import webserver.model.WebDataSource;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import java.io.IOException;
import java.io.Serializable;

@ManagedBean
@SessionScoped
public class LoginHandle implements Serializable {
    private WebDataSource ds;

    private String username;

    private String password;

    private String ruolo;

    private boolean loggedIn = false;

    public LoginHandle() throws ClassNotFoundException {
        ds = WebDataSource.getWebDataSource();
        username = null;
        password = null;
        ruolo = null;
        loggedIn = false;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void login(ActionEvent event) throws IOException {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;

        String login_act = ds.getLogin(username, password);

        //Controllo se le credenziali sono giuste
        if (login_act != null) {
            loggedIn = true;
            ruolo = login_act;
            //message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Benvenuto", username);

            switch (ruolo) {
                case "0":
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("login", username);
                    FacesContext.getCurrentInstance().getExternalContext().redirect("executive.jsf");
                    break;
                case "1":
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("login_cpm", username);
                    FacesContext.getCurrentInstance().getExternalContext().redirect("cpm.jsf");
                    break;
                case "2":
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userLogin", username);
                    FacesContext.getCurrentInstance().getExternalContext().redirect("macchina.jsf");
                    break;
            }



        } else {
            loggedIn = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Errore Login", "Credenziali inserite errate");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }


        context.addCallbackParam("loggedIn", loggedIn);
    }

    //Controllo che sia loggato e che il ruolo associato sia corretto
    public String handshakeLogin(String ruolo) {
        if (loggedIn == true && this.ruolo.equals(ruolo))
            return username;
        else
            return null;
    }

    //logout event, invalidate session
    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "index?faces-redirect=true";
    }
}