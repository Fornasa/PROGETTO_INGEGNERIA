package webserver.controller;

import webserver.model.Cpm;
import webserver.model.WebDataSource;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@ManagedBean()
@ViewScoped
public class TabellaExecutive implements Serializable {

    WebDataSource ds = null;
    private List<Cpm> cpm = new ArrayList<Cpm>();

    private String executive;

    private List<Cpm> cpmSelezionati;

    private String login;
    private String email;
    private String tel_number;

    private String cpmlogin;
    private String cpmpasswd;
    private String cpmtel_number;
    private String cpmemail;



    @PostConstruct
    public void init() {


        executive = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("login");
        System.out.println(this.getClass().getName() + "Username passato: " + executive);

        try {
            ds = WebDataSource.getWebDataSource();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        cpm = ds.getCpmFromExecutive(executive);
        System.out.println("Dimensione CPM " + cpm.size());

        login = "bosslogin";
        email = "bosslogin@gmail.com";
        tel_number = "3337573379";

    }


    public List<Cpm> getCpm() {
        return cpm;
    }

    public String outcome() {

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        //Ottengo il login del CPM che andremo a visualizzate
        String cpm_selezionato = params.get("cpm_selezionato");

        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("login_cpm", cpm_selezionato);

        System.out.println(this.getClass().getSimpleName() + "Parametro login cpm: " + cpm_selezionato);

        return "cpm?faces-redirect=true";
    }

    public String addAction() throws IOException, ClassNotFoundException {


        Cpm nuovo = new Cpm();
        nuovo.setLogin(cpmlogin);
        nuovo.setEmail(cpmemail);
        nuovo.setTel_number(cpmtel_number);
        nuovo.setExecutive(executive);

        cpm.add(nuovo);

        FacesMessage msg = new FacesMessage("Aggiunto con Successo");
        FacesContext.getCurrentInstance().addMessage(null, msg);

        //resetto i campi
        cpmlogin = null;
        cpmemail = null;
        cpmtel_number = null;

        return null;
    }


    public List<Cpm> getCpmSelezionati() {
        return cpmSelezionati;
    }

    public void setCpmSelezionati(List<Cpm> cpmSelezionati) {
        for (Cpm cpm : cpmSelezionati)
            System.out.println("Cpm selezionato: " + cpm);

        this.cpmSelezionati = cpmSelezionati;
    }

    public void cancellaCpm() {
        System.out.println("Cpm cancellato: " + cpm);
        for (Cpm cpm : cpmSelezionati)
            this.cpm.remove(cpm);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel_number() {
        return tel_number;
    }

    public void setTel_number(String tel_number) {
        this.tel_number = tel_number;
    }

    public String getCpmlogin() {
        return cpmlogin;
    }

    public void setCpmlogin(String cpmlogin) {
        this.cpmlogin = cpmlogin;
    }

    public String getCpmpasswd() {
        return cpmpasswd;
    }

    public void setCpmpasswd(String cpmpasswd) {
        this.cpmpasswd = cpmpasswd;
    }

    public String getCpmtel_number() {
        return cpmtel_number;
    }

    public void setCpmtel_number(String cpmtel_number) {
        this.cpmtel_number = cpmtel_number;
    }

    public String getCpmemail() {
        return cpmemail;
    }

    public void setCpmemail(String cpmemail) {
        this.cpmemail = cpmemail;
    }
}