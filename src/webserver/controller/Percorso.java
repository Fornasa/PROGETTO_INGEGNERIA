package webserver.controller;

import device.coordinates.Coordinates;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Polyline;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@ManagedBean
@SessionScoped
public class Percorso implements Serializable {

    public int zoom;
    @ManagedProperty(value = "#{macchinaView}")
    MacchinaView macchinaView;
    Polyline polyline = new Polyline();
    private MapModel polylineModel;
    private Date dataInizio = null;
    private Date dataFine = null;
    private String centraMappa = "0,0";

    public int getZoom() {
        return zoom;
    }

    public void setMacchinaView(MacchinaView macchinaView) {
        this.macchinaView = macchinaView;
    }

    @PostConstruct
    public void init() {

        zoom = 1;

        polylineModel = new DefaultMapModel();


        polyline.setStrokeWeight(10);
        polyline.setStrokeColor("#FF9900");
        polyline.setStrokeOpacity(0.7);

        polylineModel.addOverlay(polyline);
    }

    public MapModel getPercorsoModel() {
        return polylineModel;
    }

    public Date getDataInizio() {
        return dataInizio;
    }

    public void setDataInizio(Date dataInizio) {
        this.dataInizio = dataInizio;
    }

    public Date getDataFine() {
        return dataFine;
    }

    public void setDataFine(Date dataFine) {
        this.dataFine = dataFine;
    }

    public String getCentraMappa() {
        return centraMappa;
    }

    public void printIt(ActionEvent event) {

        polyline.getPaths().clear();


        if (dataInizio != null && dataFine != null) {
            List<Coordinates> positionsBetween = macchinaView.getPositionsBetween(new Timestamp(dataInizio.getTime()), new Timestamp(dataFine.getTime()));

            //Centro la posizione della mappa
            if (!positionsBetween.isEmpty()) {
                centraMappa = "" + positionsBetween.get(0).getLatitude() + "," + positionsBetween.get(0).getLongitude();
                zoom = 14;
            }

            System.out.println(this.getClass().getName() + "Cordinata 0: " + positionsBetween.get(0));
            Coordinates c = positionsBetween.get(0);
            boolean first = true;
            //Ottengo le coordinate
            for (Coordinates coordinate : positionsBetween) {
                if (c.equals(coordinate) && !first) {
                    break;
                }
                first = false;
                System.out.println(this.getClass().getName() + ": Aggiunta coordinata" + new LatLng(coordinate.getLatitude(), coordinate.getLongitude()));
                polyline.getPaths().add(new LatLng(coordinate.getLatitude(), coordinate.getLongitude()));
                System.out.println(coordinate);
            }
        }

    }
}
