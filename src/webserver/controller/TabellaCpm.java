package webserver.controller;

import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import webserver.model.Guidatore;
import webserver.model.WebDataSource;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@ManagedBean()
@ViewScoped
public class TabellaCpm implements Serializable {

    WebDataSource ds = null;
    private String login_cpm;
    private List<Guidatore> driverSelezionati;

    //Variabili del CPM in questione
    private List<Guidatore> guidatori;
    private String login;
    private String email;
    private String tel_number;


    //Variabili per la creazione del nuovo utente
    private String newLogin;
    private String newPasswd;
    private String newTel_number;
    private String newEmail;
    private String newActual_vehicle;
    private String newTarga;


    private String newDevice;

    private String dirigente;

    private List<String> veicoliFake;

    private String vehiclePlate;
    private String vehicleName;
    private List<Guidatore> selezione;

    @PostConstruct
    public void init() {

        //FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("login", "cfm1login");
        login_cpm = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("login_cpm");


        System.out.println("Check Tabella CPM:" + login_cpm);

        try {
            ds = WebDataSource.getWebDataSource();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        List<String> lista_guidatori = ds.getGuidatoreLoginFromCpm(login_cpm);

        guidatori = new ArrayList<Guidatore>();
        for (String driver : lista_guidatori) {
            Guidatore g = null;
            try {
                g = new Guidatore();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            g.setLogin(driver);
            guidatori.add(g);
        }

        //Valori di default
        email = "gestorepm@gmail.com";
        tel_number = "33384358594";
        login = login_cpm;

        veicoliFake = new ArrayList<String>();
        veicoliFake.add("Bentley2015");
        veicoliFake.add("RollRoyce2000");
        veicoliFake.add("Chevrolet1999");

    }

    public void cancellaGuidatore() {
        for (Guidatore driver : driverSelezionati)
            guidatori.remove(driver);
    }

    public List<String> getVeicoliFake() {
        return veicoliFake;
    }


    public List<Guidatore> getGuidatori() {
        return guidatori;
    }


    public String addAction() throws IOException, ClassNotFoundException {

        //Non utilizzata,  utilizzo fake
        //WebDataSource.getWebDataSource().insertGuidatore(newLogin, newPasswd, newTel_number, newEmail, login_cpm, actual_vehicle);

        Guidatore nuovo = new Guidatore();
        nuovo.setLogin(newLogin);
        nuovo.setTel_number(newTel_number);
        nuovo.setEmail(newEmail);
        nuovo.setVehicleName(newActual_vehicle);
        nuovo.setVehiclePlate(newTarga);
        nuovo.setDevice(newDevice);
        guidatori.add(nuovo);

        FacesMessage msg = new FacesMessage("Valori Inseriti");
        FacesContext.getCurrentInstance().addMessage(null, msg);

        //resetto i campi
        newLogin = null;
        newPasswd = null;
        newTel_number = null;
        newEmail = null;
        newActual_vehicle = null;
        newTarga = null;
        newDevice = null;

        return null;
    }


    public void onRowEdit(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Modifica Effettuata");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Modifica Cancellata");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if (newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Valore Modificato:", "Da: " + oldValue + ", A:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }


    public List<Guidatore> getGuidatoreSelezionati() {
        return driverSelezionati;
    }

    public void setGuidatoreSelezionati(List<Guidatore> driverSelezionati) {
        this.driverSelezionati = driverSelezionati;
    }

    public String getLogin() {

        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        System.out.println("Email CPM modificata:" + this.email);
        this.email = email;
    }

    public String getTel_number() {
        return tel_number;
    }

    public void setTel_number(String tel_number) {
        this.tel_number = tel_number;
    }



    public void nuovoVideo(String device) {

        try {
            ds.storeVideo("https://www.youtube.com/v/zoQH0mPDgHI?asd=" + Math.random(), device);
            System.out.println("Videlo Salvato, device: " + device);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public String getDirigente() {
        this.dirigente = "bosslogin";
        return dirigente;
    }

    public void setDirigente(String dirigente) {
        this.dirigente = dirigente;
    }

    public String getNewLogin() {
        return newLogin;
    }

    public void setNewLogin(String newLogin) {
        this.newLogin = newLogin;
    }

    public String getNewPasswd() {
        return newPasswd;
    }

    public void setNewPasswd(String newPasswd) {
        this.newPasswd = newPasswd;
    }

    public String getNewTel_number() {
        return newTel_number;
    }

    public void setNewTel_number(String newTel_number) {
        this.newTel_number = newTel_number;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public String getNewActual_vehicle() {
        return newActual_vehicle;
    }

    public void setNewActual_vehicle(String newActual_vehicle) {
        this.newActual_vehicle = newActual_vehicle;
    }

    public String getNewTarga() {
        return newTarga;
    }

    public void setNewTarga(String newTarga) {
        this.newTarga = newTarga;
    }

    public String getNewDevice() {
        return newDevice;
    }

    public void setNewDevice(String newDevice) {
        this.newDevice = newDevice;
    }
}