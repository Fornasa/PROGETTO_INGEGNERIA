package applicationServer.model;

import device.InfoGps;
import device.coordinates.Coordinates;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SimpleDeviceDataSource implements DeviceDataSource {
	
	//per implementare il singleton
	private static SimpleDeviceDataSource instance = null;
	
	private final String user = "utentebasi";
    private final String passwd = "aminsavio";

	// URL per la connessione alla base di dati e' formato dai seguenti
	// componenti: <protocollo>://<host del server>/<nome base di dati>.
    private final String url = "jdbc:postgresql://81.4.104.73:5432/basidb";

	// Driver da utilizzare per la connessione e l'esecuzione delle query.
    private final String driver = "org.postgresql.Driver";
    
    //Query per recuperare il numero di telefono del cfm dall'id del dispositivo
    private final String queryTelNumber = "SELECT C.tel_number "+
    		"FROM ing_cfm C, ing_vehicle V, ing_usr U "+
            "WHERE V.device_id=? AND U.actual_vehicle=V.plate AND U.cfm=C.login ";
    
    //Query per recuperare la email di un RPA dall'id del dispositivo
    private final String queryEmail = "SELECT C.email "+
    		"FROM ing_cfm C, ing_vehicle V, ing_usr U "+
    		"WHERE V.device_id=? AND U.actual_vehicle=V.plate AND U.cfm=C.login";
    
    //Query per recuperare il nome di un veicolo dall'id del dispositivo
    private final String queryName = "SELECT name "+
    		"FROM ing_vehicle "+
    		"WHERE device_id=?";

    //Query per ottenere tutti le velocità di allarme di un dispositivo
    private final String queryAlarms = "SELECT speed FROM ing_alarms WHERE device=? ORDER BY speed DESC";

    //Query per salvare la posizione sulla base
    private final String queryStorePos = "INSERT INTO ing_gpsdev VALUES (?,?,?,?)";
    
    //Query per salvare il video sulla base di dati
    private final String queryStoreVideo = "INSERT INTO ing_video VALUES (?,?)";
    
    //Query che restituisce tutti gli id dei deivice
    private final String queryDevicesId = "SELECT id FROM ing_device";
    
    //Query per ottenere l'executive dall'id del dispositivo
    private final String queryGetExecutiveFromId ="SELECT E.login "+
    		"FROM ing_cfm C, ing_vehicle V, ing_usr U, Executive E "+
    		"WHERE V.device_id=? AND U.actual_vehicle=V.plate AND U.cfm=C.login AND C.executive=E.login ";
    
    //Query per ottenere le ultime n posizioni
    private final String queryLastPos = "SELECT G.latitude, G.longitude, G.time "+
    		"FROM ing_gpsdev G "+
    		"WHERE G.id=? "+
    		"ORDER BY G.time DESC "+
    		"LIMIT ?";
    
  //Query per ottenere la tagra dall' id di un dispositivo 
  	private final String queryGetPlateFromId = "SELECT plate FROM ing_vehicle WHERE device_id=? ";
    
    
    //==================================METDOI===================================================
   
    private SimpleDeviceDataSource() throws ClassNotFoundException {
        try{
        	Class.forName( driver );
        }catch(ClassNotFoundException e){
        	e.printStackTrace();
        }
      }
    
    public static SimpleDeviceDataSource getDeviceDataSource() throws ClassNotFoundException {
        if (instance == null) {
            instance = new SimpleDeviceDataSource();
        }
        return instance;
    }

    //===========================METODI CREAZIONE BEAN===========================================

	public static void main(String[] args) throws ClassNotFoundException {

		SimpleDeviceDataSource ds = SimpleDeviceDataSource.getDeviceDataSource();
		/*float latitudine = 0.12345f;
		float longitudine= 0.67891f;
		Coordinates c = new Coordinates(latitudine,longitudine);

		InfoGps infogpsiniziali = new InfoGps(c,time);
		ds.storePos(infogpsiniziali,"DISP-000");
		InfoGps infogpsfinali = ds.getLastPos("DISP-000");
		System.out.println("POSIZIONE  E ORA CHE SI VOLEVA INSERIRE = "+infogpsiniziali.toString());
		System.out.println("POSIZIONE E ORA  OTTENUTA  DAL DATABASE = "+infogpsfinali.toString());
		System.out.println("I DUE VALORI DOVREBBERO ESSERE UGUALI; LO SONO? "+(infogpsiniziali.equals(infogpsfinali)?"SI!":"NO"));
		*/
		Timestamp time = new Timestamp(System.currentTimeMillis());
		System.out.println("\n=======================================\n");
		System.out.println("ULTIME 5 POSIZIONI DI DISP-000:");
		for (InfoGps i : ds.getLastNPos("DISP-000", 5))
			System.out.println("\t-" + i);
		System.out.println("\n=======================================\n");
		System.out.println("LE VELOCITA' DI ALLARME DI DISP-000:");
		for (Integer vel : ds.getAlarmsFromId("DISP-000"))
			System.out.println("\t-" + vel.toString() + "km/h");
		System.out.println("\n=======================================\n");
		System.out.println("INFORMAZIONI DEL DISPOSITIVO DISP-000: ");
		System.out.println("\t-NUMERO DI TELEFONO RPA: " + ds.getCfmTelNumberFromId("DISP-000"));
		System.out.println("\t-EMAIL RPA: " + ds.getCfmEmailFromId("DISP-000"));
		System.out.println("\t-NOME VEICOLO ASSOCIATO: " + ds.getVehicleNameFromId("DISP-000"));
		System.out.println("\n=======================================\n");
		System.out.println("TUTTI I DEVICES ID: ");
		for (String id : ds.getAllDevicesId())
			System.out.println("\t-" + id);
		System.out.println("\n=======================================\n");
		System.out.println("TIMESTAMP: " + time.getClass().getSimpleName());
	}

    private InfoGps makeInfoGpsBean(ResultSet rs) throws SQLException {
		double latitude = rs.getFloat("latitude");
		double longitude = rs.getFloat("longitude");
		Coordinates c = new Coordinates(latitude,longitude);
		Timestamp t = rs.getTimestamp("time");
		InfoGps res = new InfoGps(c, t);
		return res;
	}
    
    //===============================METODI QUERY================================================
    @Override
    public String getVehicleNameFromId (String id){

    	List<Object> args = new ArrayList<Object>();
    	String result = null;

    	args.add(id);
    	ResultSet rs = executeQuery(queryName,args,false);

    	try{
	      if( rs.next() ) {
	        result = rs.getString("name") ;
	      }

	    } catch( SQLException sqle ) { // Catturo le eventuali eccezioni
	      sqle.printStackTrace();
	    }
	    return result.trim();
	  }
    
    @Override
    public String getCfmTelNumberFromId (String id){
    	List<Object> args = new ArrayList<Object>();
    	String result = null;

		args.add(id);
    	ResultSet rs = executeQuery(queryTelNumber,args,false);

		try{
	      if( rs.next() ) {
	        result = rs.getString("tel_number") ;
	      }

	    } catch( SQLException sqle ) { // Catturo le eventuali eccezioni
	      sqle.printStackTrace();
	    }
	    return result.trim();
	  }
    
    @Override
    public String getCfmEmailFromId (String id){
    	List<Object> args = new ArrayList<Object>();
    	String result = null;

		args.add(id);
    	ResultSet rs = executeQuery(queryEmail,args,false);

		try{
	      if( rs.next() ) {
	        result = rs.getString("email") ;
	      }

	    } catch( SQLException sqle ) { // Catturo le eventuali eccezioni
	      sqle.printStackTrace();
	    }
	    return result.trim();
	  }

	@Override
    public List<Integer> getAlarmsFromId(String deviceId) {
    	List<Object> args = new ArrayList<Object>();
    	List<Integer> result = new ArrayList<Integer>();

		args.add(deviceId);
    	ResultSet rs = executeQuery(queryAlarms,args,false);

		try{
	      while( rs.next() ) {
	        result.add(rs.getInt("speed")) ;
	      }

	    } catch( SQLException sqle ) { // Catturo le eventuali eccezioni
	      sqle.printStackTrace();
	    }
	    return result;
	  }

    @Override
    public List<String> getAllDevicesId(){
    	List<Object> args = new ArrayList<Object>();
    	List<String> result = new ArrayList<String>();

		ResultSet rs = executeQuery(queryDevicesId,args,false);

		try{
	      while( rs.next() ) {
	        result.add(rs.getString("id").trim()) ;
	      }

	    } catch( SQLException sqle ) { // Catturo le eventuali eccezioni
	      sqle.printStackTrace();
	    }
	    return result;
	 }

	@Override
	public void storePos(InfoGps posTime, String deviceId) {
		List<Object>args = new ArrayList<Object>();
		args.add(posTime.getPos().getLatitude());
		args.add(posTime.getPos().getLongitude());
		args.add(posTime.getTime());
		args.add(deviceId);
		executeQuery(queryStorePos,args,true);
	}

	@Override
	public InfoGps getLastPos(String deviceId) {
		List<InfoGps> list = getLastNPos(deviceId,1);
		if (list.isEmpty()){
			System.err.println("ERRORE IN getLastPos: Impossibile recuperare ultime posizioni");
		}
		return list.get(0);
	}

	@Override
	public List<InfoGps> getLastNPos(String deviceId, int length) {
		List<Object> args = new ArrayList<Object>();
    	List<InfoGps> result = new ArrayList<InfoGps>();

		args.add(deviceId);
    	args.add((Integer)length);
    	ResultSet rs = executeQuery(queryLastPos,args,false);

		try{
	      while( rs.next() ) {
	        result.add(makeInfoGpsBean(rs)) ;
	      }

	    } catch( SQLException sqle ) { // Catturo le eventuali eccezioni
	      sqle.printStackTrace();
	    }
	    return result;
	 }
	
	@Override
	public void storeVideo(String videoPath, String deviceId) {
		List<Object> args = new ArrayList<Object>();
		String executive = "";
		args.add(deviceId);

		//PER PRIMA COSA RECUPER L'EXECUTIVE DALL' ID
		ResultSet rs1 = executeQuery(queryGetExecutiveFromId, args, false);
		try {
			if (rs1.next())
				executive = rs1.getString("login");
		} catch (SQLException sqle) { // Catturo le eventuali eccezioni
			sqle.printStackTrace();
		}
		
		//PER PRIMA COSA RECUPERA LA TARGA DALL' ID
		List<Object> args2 = new ArrayList<Object>();
		String plate ="";
		args2.add(deviceId);
		ResultSet rs2 = executeQuery(queryGetPlateFromId, args2, false);
		try {
			if (rs2.next())
				plate = rs2.getString("plate");
			} catch (SQLException sqle) { // Catturo le eventuali eccezioni
				sqle.printStackTrace();
		}

		//CONTROLLO DI ESSERE RIUSCITO A TROVARE L'EXECUTIVE
		if (executive == null)
			return;
		//USO L'EXECUTIVE PER POPOLARE LA TABELLA

		args = new ArrayList<Object>();
		args.add(executive);
		args.add(videoPath);
		args.add(plate);
		executeQuery(queryStoreVideo, args, true);
	}
	
	/**
	 *
	 * @param query la query da eseguire
	 * @param args la lista di parametri in caso usare lista vuota. NB castare a container
	 * @param isUpdate se la query è una update
	 * @return se la query è una update ritorna null
	 */
	private ResultSet executeQuery (String query, List<Object> args, boolean isUpdate){
		Connection con = null;
		PreparedStatement pstmt = null;
	    ResultSet rs = null;
	    int index = 1;

	    try {
	      // tentativo di connessione al database
	      con = DriverManager.getConnection( url, user, passwd );
	      // connessione riuscita, ottengo l'oggetto per l'esecuzione dell'interrogazione.
	      pstmt = con.prepareStatement( query );
	      pstmt.clearParameters();
	      // imposto i parametri della query
	      for(Object param:args){
	    	 if(param.getClass().getSimpleName().equals("Integer"))
	    		 pstmt.setInt(index, (int)param);
	    	 else if(param.getClass().getSimpleName().equals("Float"))
	    		 pstmt.setFloat(index, (float)param);
	    	 else if(param.getClass().getSimpleName().equals("Timestamp"))
	    		 pstmt.setTimestamp(index, (Timestamp)param);
	    	 else if(param.getClass().getSimpleName().equals("Double")){
	    		 Double d = (Double)param;
	    		 pstmt.setFloat(index, d.floatValue());
	    	 }
	    	 else{
	    		 pstmt.setString(index, (String)param);
	    	 }
	    	 index++;
	      }
	      if(isUpdate)
	    	  pstmt.executeUpdate();
	      else
	    	  rs = pstmt.executeQuery();

		} catch( SQLException sqle ) { // Catturo le eventuali eccezioni
	      sqle.printStackTrace();

	    } finally { // alla fine chiudo la connessione.
	      try {
	        con.close();
	      } catch( SQLException sqle1 ) {
	        sqle1.printStackTrace();
	      }
	    }
	    return rs;

	}
}
























