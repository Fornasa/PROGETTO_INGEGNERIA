package applicationServer.model;

import java.util.List;

import device.InfoGps;

public interface DeviceDataSource {
	
	/**
	 * Ritorna il nome di un veicolo 
	 * @param id del disposito montato su veicolo
	 * @return
	 */
	String getVehicleNameFromId (String id);
	
	/**
	 * Ritorna il numero di telefono di un RPA
	 * @param id del dispositvo posseduto dal RPA
	 * @return
	 */
	String getCfmTelNumberFromId (String id);
	
	/**
	 * Ritorna l'email di un RPA
	 * @param id del dispositivo posseduto dal RPA
	 * @return
	 */
	String getCfmEmailFromId (String id);
	
	/**
	 * Dato un id ritorna tutte le velocità di allarme di quel dispositivo
	 * @param id
	 * @return
	 */
	List<Integer> getAlarmsFromId (String id);
	
	/**
	 * Ritorna tutti i gli id dei device sulla base di dati
	 * @return tutti gli id dei device
	 */
	List<String> getAllDevicesId (); 
	
	/**
	 * Salva l'ultima posizione 
	 * @param last
	 * @param deviceId
	 */
	void storePos(InfoGps posTime, String deviceId);
	
	/**
	 * Salva il video inviato da quel dispositivo
	 * @param videoPath
	 * @param deviceId
	 */
	void storeVideo(String videoPath, String deviceId);
	
	/**
	 * Ritorna l'ultima posizione di quel dispositivo
	 * @param deviceId
	 * @return
	 */
	InfoGps getLastPos(String deviceId);
	
	/**
	 * Ritorna le ultime n posizioni di quel dispositivo
	 * @param deviceId
	 * @param length
	 * @return
	 */
	List<InfoGps> getLastNPos(String deviceId, int length);
}
