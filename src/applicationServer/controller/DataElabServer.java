package applicationServer.controller;

import java.util.Observer;

/**
 * Classe che elabora i dati ricevuti dai dispositivi
 * Si occupa di salvare i dati sulla posizione sul db
 * Controllare se il veicolo è in movimento e in caso inviare msg
 * Controllare velocità veicolo e in caso inviare msg allarme
 * 
 * @author Amin
 */
public interface DataElabServer extends Observer {
	
	/**
	 * Calcola l'ultima velocità del dispositivo
	 * @param deviceId: l'id del dispositivo
	 * @return la velocità in km/h
	 */
	public double calculateSpeed(String deviceId);
	
	/**
	 * Ritorna true se il dispositivo è in movimento false altrimenti
	 * @param deviceId: l'id del dispositivo
	 * @return true se il dispositivo è in movimento, false altrimenti
	 */
	public boolean hasStarted(String deviceId, double currentSpeed);
	
	
}
