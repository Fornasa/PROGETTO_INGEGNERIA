package applicationServer.controller;

import device.InfoGps;

public interface SpeedStrategy {
	
	public double calculateSpeed(InfoGps pos1, InfoGps pos2);

}
