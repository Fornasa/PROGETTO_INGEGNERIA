package applicationServer.controller;

import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.List;
import java.util.Observable;

import applicationServer.model.DeviceDataSource;
import applicationServer.model.SimpleDeviceDataSource;

import device.Device;
import device.InfoGps;
import device.SimpleDevice;
import device.builder.Director;

public class SimpleDataElabServer implements DataElabServer{
	
	/**
	 * Mappa usata come cache per tenere a mente l'ultima velocità registrata
	 * e vedere se un veicolo è fermo per ogni dispositivo
	 */
	private final Hashtable<String, Double> lastSpeeds = new Hashtable<String, Double>();

	/**
	 * Mappa usata per tenere a mente quanto aspetta ogni device tra la registrazione
	 * di una posizione e l'altra, questa potrebbe variare nel tempo per cui ci teniamo
	 * a mente il dispositivo
	 */
	private final Hashtable<String, Device> observedDevices = new Hashtable<String, Device>();
	
	/**
	 * Oggetto usato per accedere ai dati sul db che contiene i dati del device
	 */
	private final DeviceDataSource deviceDS;
	private final SpeedStrategy speedCalculator;
	
	
	/**
	 * Nella rilevare due posizioni in un intervallo di tempo, il dispositivo potrebbe
	 * commettere un errore nei tempi, questa variabile tiene il max tempo di errore
	 */
	private static long TIME_INTERVAL_ERROR = 2000;
	
	
	private SimpleDataElabServer(DeviceDataSource ds, SpeedStrategy speedCalculator, 
								List<Device> devices) {
		this.deviceDS = ds;
		this.speedCalculator = speedCalculator;
		
		for (Device d : devices) {
			observedDevices.put(d.getId(), d);
			d.addObserver(this);
		}
	}		
	
	/**
	 * Registra la posizione sul db usando il DeviceDataSource
	 */
	@Override
	public void update(Observable device, Object infoGps) throws IllegalArgumentException{
		
		if (!(device instanceof SimpleDevice))
			throw new IllegalArgumentException(device + " non è un device!");
		
		Device deviceAsDev = (Device) device;
		
		if (!(infoGps instanceof InfoGps))
			throw new IllegalArgumentException(infoGps + " non è una InfoGps!");
		
		InfoGps infoGpsAsInfo = (InfoGps) infoGps;
		
		String deviceId = deviceAsDev.getId();
		
		deviceDS.storePos(infoGpsAsInfo, deviceId);
		
		double speed = calculateSpeed(deviceId);
		
		// faccio il check che il dispositivo sia in movimento mentre prima era fermo
		if (hasStarted(deviceId, speed))
			sendSms(deviceId, "in movimento");
		
		// faccio il check che il dispositivo non abbia superato il limite
		double limit = limitExceeded(deviceId, speed);
		
		if (limit != -1)
			sendLimitExceededSms(deviceId, speed, limit);
	}
	
	/**
	 * Controlla che non sia stato superato nessun limite da parte del veicolo
	 * che viaggia alla velociatà corrente
	 * @param deviceId
	 * @param currentSpeed
	 * @return la velocità di allarme superata o -1 in caso non sia stato superato nessun allarme
	 */
	private double limitExceeded(String deviceId, double currentSpeed) {
		List<Integer> speedLimits = deviceDS.getAlarmsFromId(deviceId);
		
		for (Integer limit : speedLimits) {
			System.out.println("DEBUG: id: " + deviceId + " limit: " + limit);
			if (currentSpeed > limit.doubleValue())
				return limit.doubleValue();
		}
		
		return -1;
	}
	
	private void sendLimitExceededSms(String deviceId, double speed, double speedLimit) {
		
		System.out.println("DEBUG: \t" + deviceId);
		
		String msg = "superato il limite di " + speedLimit + ", velocità attuale: " + speed;
		
		sendSms(deviceId, msg);
	}
	
	private void sendSms(String deviceId, String msg) {
		String number = deviceDS.getCfmTelNumberFromId(deviceId);
		
		String vehicleId = deviceDS.getVehicleNameFromId(deviceId);
		
		msg = vehicleId + " - " + msg;
		
		callSmsAPI(number, msg);
	}
	
	private void callSmsAPI(String number, String msg) {
		System.out.println("Sending sms\n\tNumber<" + number.trim() + ">\n\tMessage: " + msg);
	}

	/**
	 * Prende le ultime due posizioni i relativi tempi e calcola la velocità
	 * usata per andare da un all'altra 
	 */
	@Override
	public double calculateSpeed(String deviceId) {
		
		List<InfoGps> lastInfos = deviceDS.getLastNPos(deviceId, 2); // get last 2 coordinates saved
		
		if (lastInfos.size() != 2)
			return 0.0f;
		
		System.out.println("DEBUG:\tcalculating speed between " + 
				"\n\t" + lastInfos.get(1) + "\n\t" + lastInfos.get(0));
		
		Timestamp time1 = lastInfos.get(1).getTime();
		Timestamp time2 = lastInfos.get(0).getTime();
		
		long elapsedTime = time2.getTime() - time1.getTime();
		Device checkedDev = observedDevices.get(deviceId);
		
		if (checkedDev == null)
			throw new IllegalStateException("unkown device " + deviceId);
		
		long timeInterval = checkedDev.getLastTimeInterval();
		
		System.out.println("DEBUG:\trilevation interval is  " + elapsedTime + 
							"and correct interval is " + timeInterval);
		
		if (elapsedTime-TIME_INTERVAL_ERROR > timeInterval) // calcolare la velocità ha senso 
			return 0.0f;									// solo se sono due rilevazioni in
															// sequenza, altrimenti non serve
		
		System.out.println("DEBUG:\tcalculating speed between " + 
							"\n\t" + lastInfos.get(1) + "\n\t" + lastInfos.get(0));
		
		return speedCalculator.calculateSpeed(lastInfos.get(1), lastInfos.get(0));
	}
	
	@Override
	public boolean hasStarted(String deviceId, double currentSpeed) {
		Double lastSpeed = lastSpeeds.get(deviceId);
		
		System.out.println("DEBUG:\n\tdevice: " + deviceId 
							+ "\n\tcurrent speed: " + currentSpeed
							+ "\n\tlast speed: " + lastSpeed);
		
		lastSpeeds.put(deviceId, new Double(currentSpeed));
		
		if (lastSpeed == null)
			return true;
		
		
		
		return (lastSpeed.doubleValue() == 0.0f && currentSpeed != 0.0f);
	}
	
	public static void main(String[] args) {
		
		DeviceDataSource ds = null;
		
		try {
			ds = SimpleDeviceDataSource.getDeviceDataSource();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		Director dir = null;
		try {
			dir = new Director();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		List<Device> devices = dir.constructorDevices();
		
		DataElabServer server = new SimpleDataElabServer(ds, new HaversineSpeed(), devices);
		
		for (Device d:devices)
			new Thread(d).start();
		
	}

}
