package applicationServer.controller;

import java.sql.Timestamp;

import device.InfoGps;
import device.coordinates.CoordinatesInterface;

public class HaversineSpeed implements SpeedStrategy {

	@Override
	public double calculateSpeed(InfoGps pos1, InfoGps pos2) {
		
		double distanceInKm = getDistance(pos1.getPos(), pos2.getPos());
		double timeInHours = getTimeInHours(pos1.getTime(), pos2.getTime());
		
		System.out.println("DEBUG: distance between "
							+ "\n" + pos1 + "\n" + pos2
							+ " is " + distanceInKm
							+ " and time is " + timeInHours);
		
		return distanceInKm/timeInHours;
	}
	
	private double getDistance(CoordinatesInterface pos1, CoordinatesInterface pos2) {
		double R = 6378137.0f; // Earth’s mean radius in meters
		double dLat = degToRad(pos2.getLatitude() - pos1.getLatitude());
		double dLong = degToRad(pos2.getLongitude() - pos1.getLongitude());
		double a = Math.sin(dLat / 2.0f) * Math.sin(dLat / 2.0f) +
				Math.cos(degToRad(pos1.getLatitude())) * Math.cos(degToRad(pos2.getLatitude())) *
				Math.sin(dLong / 2.0f) * Math.sin(dLong / 2.0f);
		double c = 2.0f * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double d = R * c;
		  
		return d/1000.0f; // returns the distance in kilometer
	}
	

	
	
	private double getTimeInHours(Timestamp time1, Timestamp time2) {
		
		long timeInMilliseconds = time2.getTime() - time1.getTime();
		
		return ((double) timeInMilliseconds)/3600000.0f;
	}
	
	private double degToRad(double measure) {
		return (measure * Math.PI / 180.0);
	}
	
	private double radToDeg(double measure) {
		return (measure * 180 / Math.PI);
	}
	

}
