package device;

import java.util.Observable;

public abstract class Device extends Observable implements Runnable {

	public abstract String getId();
	
	public abstract int getLastTimeInterval();
	
}
