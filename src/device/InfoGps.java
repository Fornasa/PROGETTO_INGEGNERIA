package device;

import java.sql.Timestamp;

import device.coordinates.Coordinates;

public class InfoGps {
	
	private final Coordinates pos;
	private final Timestamp time;
	
	public InfoGps(Coordinates pos, Timestamp time) {
		this.pos = pos;
		this.time = time;
	}
	
	/**
	 * @return the pos
	 */
	public Coordinates getPos() {
		return pos;
	}

	/**
	 * @return the time
	 */
	public Timestamp getTime() {
		return time;
	}
	
	public boolean equals(InfoGps other){
		return pos.equals(other.getPos()) && time.equals(other.getTime());
	}
	
	public String toString(){
		return "Coordinate: "+pos.toString()+" Tempo: "+time.toString();
	}
}
