package device.coordinates;

public interface CoordinatesInterface {

	/**
	 * @return the latitutide
	 */
	public String toMapsFormat();
	/**
	 * @return the longitude
	 */
	public double getLongitude();
	/**
	 * @return the format string for maps
	 */
	public double getLatitude();
	/**
	 * return true if other is equal to this
	 * @param other
	 * @return 
	 */
	public boolean equals(Coordinates other);
	/**
	 * @return String of coordinates
	 */
	public String toString();
}
