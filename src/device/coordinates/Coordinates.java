package device.coordinates;


public class Coordinates implements CoordinatesInterface{
	
	private final double latitude;
	private final double longitude;
		
	public Coordinates(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}
	
	public String toMapsFormat(){
		return latitude + ", " + longitude;
	}
	
	public boolean equals(Coordinates other){
		return latitude==other.getLatitude() && longitude == other.getLongitude();
	}
	
	public String toString(){
		return latitude + ", " + longitude;
	}
}
