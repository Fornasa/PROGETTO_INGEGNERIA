package device.coordinates;

import org.junit.Test;

public class CoordinatesTest {

	@Test
	public void testCoordinates() {
		Coordinates cordinate= new Coordinates(1.0,1.0);
		assert(cordinate != null);
	}

	@Test
	public void testGetLatitude() {
		Coordinates cordinate= new Coordinates(1.0,2.0);
		assert(cordinate.getLatitude()==1.0);
	}

	@Test
	public void testGetLongitude() {
		Coordinates cordinate= new Coordinates(1.0,2.0);
		assert(cordinate.getLongitude()==2.0);
	}

	@Test
	public void testToMapsFormat() {
		Coordinates cordinate= new Coordinates(1.0,2.0);
		assert(cordinate.toMapsFormat() instanceof String);
	}

	@Test
	public void testToString() {
		Coordinates cordinate= new Coordinates(1.0,2.0);
		assert(cordinate.toString() instanceof String);	}

}
