package device.video;

public interface CameraInterface  {
	
	/**
	 * Start recording
	 */
	public void startRecording();
	
	/**
	 * Return path to video
	 * @return path
	 */
	public String getVideo();
}
