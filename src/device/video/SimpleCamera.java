package device.video;

import java.util.Observable;

public class SimpleCamera extends Observable implements CameraInterface {

	private String video;
	
	
	public SimpleCamera(){
		this.video=null;
	}
	
	@Override
	public void startRecording(){
		this.video="path del video";
		this.setChanged();
		notifyObservers();
	}

	@Override
	public String getVideo(){
		return video;
	}
}
