package device.video;

import org.junit.Test;

public class SimpleCameraTest {

	@Test
	public void testSimpleCamera(){
		SimpleCamera camera = new SimpleCamera();
		assert(camera.getVideo()==null);
	}
	
	@Test
	public void testStartRecording() {
		SimpleCamera camera = new SimpleCamera();
		camera.startRecording();
		assert(camera.hasChanged() == true);	
	}

	@Test
	public void testGetVideo() {
		SimpleCamera camera = new SimpleCamera();
		camera.startRecording();
		assert(camera.getVideo() instanceof String);
		
	}

}
