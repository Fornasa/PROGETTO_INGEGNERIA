package device;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;
import java.util.Date;
import java.util.Set;

import device.antenna.AntennaInterface;
import device.coordinates.Coordinates;
import device.video.CameraInterface;
import device.video.SimpleCamera;

public class SimpleDevice extends Device implements Observer{
	
	private CameraInterface camera;
	private AntennaInterface antenna;
	private Set<String> video= new HashSet<String>();
	private final String id;

	public SimpleDevice(AntennaInterface antenna, int TempoSimulazione, String id, CameraInterface camera){
		this.antenna = antenna;
		this.TempoSimulazione = TempoSimulazione * 1000; //conversione in millisecondi
		this.id = id;
		this.camera=camera;
	}

	
	public Set<String> getVideo() {
		return video;
	}

	public void deleteVideo(String video){
		this.video.remove(video);
	}
	
	// tempo in millisecondi prima di controllare il prossimo valore sul GPS
	static private int TempoIntervallo = 3000;
	
	// tempo da simulare in millisecondi
	private int TempoSimulazione;


	public SimpleDevice(String id, AntennaInterface antenna){
		this.id = id;
		this.antenna=antenna;
	}

	public void setTempoSimulazione(int second){
		this.TempoSimulazione = second *1000;
	}
	
	public int getTempoSimulazione(){
		return this.TempoSimulazione/1000;
	}
	
	@Override
	public void update(Observable o, Object arg) {
		SimpleCamera camera= (SimpleCamera) o;
		video.add(camera.getVideo());
	}
	
	public void run(){
		for(int time=0; time <= TempoSimulazione; time+=TempoIntervallo){
			Coordinates coordinates = antenna.getCoordinate();
			Date date= new Date();
			InfoGps info = new InfoGps(coordinates, new Timestamp(date.getTime()));
			if(antenna.isUpConnection()){
				setChanged();
				notifyObservers(info);
			}
			
			try {
				Thread.sleep(TempoIntervallo);
			} catch (InterruptedException e) {	
				e.printStackTrace();
			}
		}	
	}

	public String getId() {		return id;  	}
	
	public int getLastTimeInterval() {
		return TempoIntervallo;
	}
	
}
