package device.builder;

import device.SimpleDevice;
import device.antenna.SimpleAntenna;
import device.video.SimpleCamera;

public class ConcreteBuilder extends Builder {
	
	SimpleAntenna antenna;
	SimpleCamera camera;
	
	@Override
	public void buildAntenna() {
		this.antenna = new SimpleAntenna(true);
	}

	@Override
	public void buildCamera() {
		this.camera=new SimpleCamera();
	}

	@Override
	public void buildDevice(String id, int secondiDaSimulare) {
		device=new SimpleDevice(this.antenna, secondiDaSimulare, id, camera);
	}
}
