package device.builder;

import device.Device;

public abstract class Builder {
	
	protected Device device;
	
	
	public Device getDevice(){
		return device;
	}
		
	public abstract void buildDevice(String id, int secondiDaSimulare);
		
	public abstract void buildAntenna();
	
	public abstract void buildCamera();
	
}
