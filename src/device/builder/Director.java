package device.builder;

import java.util.ArrayList;
import java.util.List;

import applicationServer.controller.DataElabServer;
import applicationServer.model.DeviceDataSource;
import applicationServer.model.SimpleDeviceDataSource;

import device.Device;

public class Director {
	
	private List<Device> devices;
	private List<Builder> builders;
	private int secondiDaSimulare;
	private List<String> ids;
	private DeviceDataSource ds;
	
	public Director() throws ClassNotFoundException{
		
		this.builders= new ArrayList<Builder>();
		this.devices = new ArrayList<Device>();
		this.ds= SimpleDeviceDataSource.getDeviceDataSource();
		this.secondiDaSimulare=36;
	}
	
	public List<Device> constructorDevices(){
		
		ids = ds.getAllDevicesId();
		
		for(int i = 0; i < 4; i++)
			builders.add(new ConcreteBuilder());
		
		int deviceIndex = 0;
		
		for(Builder b: builders){
			b.buildAntenna();
			b.buildCamera();
			b.buildDevice(ids.get(deviceIndex++), secondiDaSimulare);
			Device device = b.getDevice();
			devices.add(device);
		}
		
		return devices;
	}
}
