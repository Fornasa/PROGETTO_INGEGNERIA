package device;

import java.sql.Timestamp;
import java.util.Date;

import org.junit.Test;

import device.coordinates.Coordinates;

public class InfoGpsTest {

	@Test
	public void testInfoGps() {
		Date date= new Date();
		InfoGps info = new InfoGps(new Coordinates(2,3), new Timestamp(date.getTime()));
		assert(info != null);
	}

	@Test
	public void testGetPos() {
		Date date= new Date();
		InfoGps info = new InfoGps(new Coordinates(2,3), new Timestamp(date.getTime()));
		Coordinates coordinates = info.getPos();
		assert(coordinates.getLatitude() == 2 && coordinates.getLongitude() == 3);
	}

	@Test
	public void testGetTime() {
		Date date= new Date();
		InfoGps info = new InfoGps(new Coordinates(2,3), new Timestamp(date.getTime()));
		Timestamp time = info.getTime();
		assert(time.compareTo(date)==0);
	}

}
