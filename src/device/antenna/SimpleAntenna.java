package device.antenna;

import device.coordinates.Coordinates;

public class SimpleAntenna implements AntennaInterface {

	// Valore che rappresenta lo stato dell'antenna (se true c'e connessione)
	private boolean isUp;
	// Valore che rappresenta il percorso prestabilito
	private int percorso;
	// Valore per memorizzare lo step del percorso
	private int step;
	
	// Counter per la differenziazione dei percorsi
	static private int counter = 0;
	// Percorsi prestabiliti per la simulazione
	static final private Coordinates[][] path = {
		// Gorizia 210 km/h
		{new Coordinates(45.913667, 13.599722), new Coordinates(45.914760, 13.601334), new Coordinates(45.915847, 13.602947),
			new Coordinates(45.916811, 13.604840), new Coordinates(45.917580, 13.606850), new Coordinates(45.918144, 13.609013),
				new Coordinates(45.918480, 13.611331), new Coordinates(45.918664, 13.613651), new Coordinates(45.918872, 13.615971),
					new Coordinates(45.919066, 13.618209), new Coordinates(45.919234, 13.620466), new Coordinates(45.919445, 13.622786)},
		// Milano 30 km/h		
		{new Coordinates(45.483645, 9.201901), new Coordinates(45.483267, 9.201527), new Coordinates(45.483078, 9.201332),
				new Coordinates(45.482886, 9.201167), new Coordinates(45.482695, 9.201003), new Coordinates(45.482502, 9.200839),
					new Coordinates(45.482311, 9.200677), new Coordinates(45.482133, 9.200482), new Coordinates(45.481957, 9.200277),
						new Coordinates(45.481776, 9.200085), new Coordinates(45.481595, 9.199900), new Coordinates(45.481406, 9.199703)},
		// Roma 43 km/h		
		{new Coordinates(41.908574, 12.511982), new Coordinates(41.908340, 12.512278), new Coordinates(41.908102, 12.512578), new Coordinates(41.907864, 12.512871),
			new Coordinates(41.907626, 12.513165), new Coordinates(41.907391, 12.513464), new Coordinates(41.907150, 12.513762), new Coordinates(41.906913, 12.514054),
				new Coordinates(41.906665, 12.514348), new Coordinates(41.906430, 12.514643), new Coordinates(41.906187, 12.514936), new Coordinates(41.905952, 12.515226)},
				
		// A14 100 km/h		
		{new Coordinates(44.195147, 12.224824), new Coordinates(44.194782, 12.225740), new Coordinates(44.194406, 12.226628), new Coordinates(44.194035, 12.227514),
			new Coordinates(44.193666, 12.228395), new Coordinates(44.193279, 12.229284), new Coordinates(44.192916, 12.230168), new Coordinates(44.192556, 12.231064),
				new Coordinates(44.192195, 12.231966), new Coordinates(44.191837, 12.232860), new Coordinates(44.191481, 12.233767), new Coordinates(44.191119, 12.234634)}		
			
	
	};
	/**
	 * Costruttore di Antenna
	 * @param isUp Stato della connessione
	 */
	public SimpleAntenna(boolean isUp){
		this.isUp=isUp;
		percorso=counter;
		counter = (counter + 1) % 4;
		step=0;
	}
	
	@Override
	public boolean isUpConnection() {
		return isUp;
	}

	public void SetOnConnection(){
		isUp=true;
	}
	
	public void SetOffConnection(){
		isUp=false;
	}
	
	@Override
	public Coordinates getCoordinate() {
		if((step + 1) >= path[percorso].length )
			return path[percorso][step];
			
		return path[percorso][step++];
	}

	public static void main(String[] args){
		SimpleAntenna antenna = new SimpleAntenna(true);
		System.out.println("Primo percorso:");
		for(int i=0;i<13;i++)
			System.out.println(antenna.getCoordinate());

		antenna = new SimpleAntenna(true);
		System.out.println("Secondo percorso:");
		for(int i=0;i<13;i++)
			System.out.println(antenna.getCoordinate());

		
		antenna = new SimpleAntenna(true);
		System.out.println("Terzo percorso:");
		for(int i=0;i<13;i++)
			System.out.println(antenna.getCoordinate());

		
		antenna = new SimpleAntenna(true);
		System.out.println("Quarto percorso:");
		for(int i=0;i<13;i++)
			System.out.println(antenna.getCoordinate());
	
	}
}
