package device.antenna;

import device.coordinates.Coordinates;

public interface AntennaInterface {
	
		/**
		 * Metodo che restituisce lo stato della connesione
		 * @return true se la connesione è online
		 */
		public boolean isUpConnection();
		
		/**
		 * Metodo che restituisce le coordinate provenienti dal satellite
		 * @return cordinate del veicolo
		 */
		public Coordinates getCoordinate();

}
