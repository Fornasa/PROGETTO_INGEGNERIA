package device.antenna;

import org.junit.Test;

import device.coordinates.Coordinates;

public class SimpleAntennaTest {
	
	@Test
	public void testConstructor() {
		SimpleAntenna antenna= new SimpleAntenna(true);
		assert(antenna != null);
	}

	@Test
	public void testIsUpConnection() {
		SimpleAntenna antenna= new SimpleAntenna(false);
		assert(antenna.isUpConnection()==false);
	}
	
	@Test
	public void testSetOnConnection() {
		SimpleAntenna antenna= new SimpleAntenna(false);
		antenna.SetOnConnection();
		assert(antenna.isUpConnection()==true);
	}

	@Test
	public void testSetOffConnection() {
		SimpleAntenna antenna= new SimpleAntenna(true);
		antenna.SetOffConnection();
		assert(antenna.isUpConnection()==false);
	}

	@Test
	public void testGetCoordinates() {
		SimpleAntenna antenna= new SimpleAntenna(true); //this should have the first path
		assert(antenna.getCoordinate() instanceof Coordinates);
	}
	
	@Test
	public void testGetCoordinatesOverflow(){
		SimpleAntenna antenna= new SimpleAntenna(true); //this should have the second path
		for(int i=0;i<19;i++) // shure OverFlow
			antenna.getCoordinate();
		
		assert(antenna.getCoordinate() instanceof Coordinates);
	}
	
}
